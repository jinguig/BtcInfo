package com.szxx.model;

public class JinSeCaiJing {

	private int id;// 编号

	private String title;// 标题

	private String date;// 日期

	private String tags;// 标签

	private String author;//作者

	private String content;//内容



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "JinSeBean{" +
				"id=" + id +
				", title='" + title + '\'' +
				", date='" + date + '\'' +
				", tags='" + tags + '\'' +
				", author='" + author + '\'' +
				", content='" + content + '\'' +
				'}';
	}
}
