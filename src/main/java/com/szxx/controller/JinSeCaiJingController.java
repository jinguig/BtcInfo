package com.szxx.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.szxx.model.JinSeCaiJing;
import com.szxx.service.IJinSeCaiJingService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/JinSeCaiJing")
public class JinSeCaiJingController{
    @Resource
    private IJinSeCaiJingService iJinSeCaiJingService;

    @RequestMapping("/add")
    public void add(HttpServletRequest request, HttpServletResponse response) throws IOException{
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        JinSeCaiJing jinSeCaiJing = new JinSeCaiJing();
        jinSeCaiJing.setAuthor("justin");
        jinSeCaiJing.setContent("15465154554646");
        jinSeCaiJing.setDate("2018-09-06");
        jinSeCaiJing.setTags("php");
        jinSeCaiJing.setTitle("55665566");
        int i = iJinSeCaiJingService.add(jinSeCaiJing);
        System.out.println(i);
        ObjectMapper mapper = new ObjectMapper();
        response.getWriter().write(mapper.writeValueAsString(jinSeCaiJing));
        response.getWriter().close();
    }
}
