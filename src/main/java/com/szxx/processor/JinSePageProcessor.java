package com.szxx.processor;

import com.szxx.model.JinSeCaiJing;
import com.szxx.service.IJinSeCaiJingService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.monitor.SpiderMonitor;
import us.codecraft.webmagic.processor.PageProcessor;
import java.util.List;

@Controller
@RequestMapping("/pageProcessor")
public class JinSePageProcessor implements PageProcessor {

    private IJinSeCaiJingService iJinSeCaiJingService;
    ApplicationContext  applicationContext = new ClassPathXmlApplicationContext("classpath:spring-mybatis.xml");

    private static String username = "qq598535550";// 设置csdn用户名
    private static int size = 0;// 共抓取到的文章数量

    // 抓取网站的相关配置，包括：编码、抓取间隔、重试次数等
    private Site site = Site.me().setRetryTimes(3).setSleepTime(1000);


    public Site getSite() {
        return site;
    }


    // process是定制爬虫逻辑的核心接口，在这里编写抽取逻辑
    public void process(Page page) {
        // 列表页
        if (!page.getUrl().regex("https://www\\.jinse\\.com/bitcoin/\\d+\\.html").match()) {

            // 添加所有文章页

            page.addTargetRequests(page.getHtml().xpath("//div[@class='con']")
                    .links().regex("https://www\\.jinse\\.com/bitcoin/\\d+\\.html")
                    .all());// 限定文章列表获取区域
            page.addTargetRequests(page.getHtml().links().regex("https://www\\.jinse\\.com/bitcoin/\\w+").all());

            // 文章页
        } else {
            size++;
            //内容
            String content = page.getHtml().xpath("//div[@class='js-article']/p|//div[@class='js-article']/h2|//table|//strong|//section|//div[@class='js-article']/ul").all().toString().replaceAll(",", "");
            JinSeCaiJing jinSeCaiJing = new JinSeCaiJing();
            // 设置编号
            jinSeCaiJing.setId(Integer.parseInt(
                    page.getUrl().regex("https://www\\.jinse\\.com/bitcoin/(\\d+)\\.html").get()));
            // 设置标题
            jinSeCaiJing.setTitle(page.getHtml().xpath("//div[@class='title']/h2/text()").get());
            //设置内容
            jinSeCaiJing.setContent(content.substring(1,content.length()-1));
           // jinSeBean.setContent(page.getHtml().xpath("//div[@class='js-article']").regex("^ class=\\\"rticle-info\\\">.*<div $").get());

            //设置日期
            jinSeCaiJing.setDate(page.getHtml().xpath("//div[@class='time']/text()").get());
            // 设置标签（可以有多个，用,来分割）
            jinSeCaiJing.setTags(listToString(page.getHtml()
                    .xpath("//div[@class='tags']/a/text()").all()));
            //设置作者
            jinSeCaiJing.setAuthor(page.getHtml().xpath("//div[@class='source']//span[@class='left']/text()")
                    .regex("文章作者：(.*)").get().trim());

            // 把对象存入数据库
            iJinSeCaiJingService = (IJinSeCaiJingService) applicationContext.getBean("jinSeCaiJingService");
            iJinSeCaiJingService.add(jinSeCaiJing);

            // 把对象输出控制台
            System.out.println(jinSeCaiJing);
        }
    }

    // 把list转换为string，用,分割
    public static String listToString(List<String> stringList) {
        if (stringList == null) {
            return null;
        }
        StringBuilder result = new StringBuilder();
        boolean flag = false;
        for (String string : stringList) {
            if (flag) {
                result.append(",");
            } else {
                flag = true;
            }
            result.append(string);
        }
        return result.toString();
    }



    public static void main(String[] args) throws Exception{
        //添加监控
        Spider jinSeSpider = Spider.create(new JinSePageProcessor())
                .addUrl("http://www.jinse.com/bitcoin");
        SpiderMonitor.instance().register(jinSeSpider);
        jinSeSpider.start();
        long startTime, endTime;
        System.out.println("【爬虫开始】请耐心等待一大波数据到你碗里来...");
        startTime = System.currentTimeMillis();
        // 从用户博客首页开始抓，开启5个线程，启动爬虫
        Spider.create(new JinSePageProcessor()).addUrl("https://www.jinse.com/bitcoin").thread(5).run();
        endTime = System.currentTimeMillis();
        System.out.println("【爬虫结束】共抓取" + (size-1) + "篇文章，耗时约" + ((endTime - startTime) / 1000) + "秒，已保存到数据库，请查收！");
    }
}
