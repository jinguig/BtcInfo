package com.szxx.dao;


import com.szxx.model.User;

public interface IUserDao {

    User selectUser(long id);

}
