package com.szxx.service;

import com.szxx.model.User;

public interface IUserService {

    public User selectUser(long userId);

}
