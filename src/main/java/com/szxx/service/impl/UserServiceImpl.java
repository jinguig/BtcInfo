package com.szxx.service.impl;



import com.szxx.dao.IUserDao;

import com.szxx.model.User;
import com.szxx.service.IUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("userService")
public class UserServiceImpl implements IUserService {

    @Resource
    private IUserDao userDao;

    public User selectUser(long userId) {
        return userDao.selectUser(userId);
    }

}
