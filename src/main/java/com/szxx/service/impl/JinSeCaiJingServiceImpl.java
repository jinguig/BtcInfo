package com.szxx.service.impl;

import com.szxx.dao.IJinSeCaiJingDao;
import com.szxx.model.JinSeCaiJing;
import com.szxx.service.IJinSeCaiJingService;
import org.springframework.stereotype.Service;
import us.codecraft.webmagic.Site;


import javax.annotation.Resource;


@Service("jinSeCaiJingService")
public class JinSeCaiJingServiceImpl implements IJinSeCaiJingService{
    @Resource
    private IJinSeCaiJingDao iJinSeCaiJingDao;

    public int add(JinSeCaiJing jinSeCaiJing) {
        return iJinSeCaiJingDao.add(jinSeCaiJing);
    }
}
