import com.szxx.dao.IJinSeCaiJingDao;
import com.szxx.model.JinSeCaiJing;
import com.szxx.service.IJinSeCaiJingService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring-mybatis.xml"})
public class IJinSeCaiJingDaoTest {
    @Autowired
    private IJinSeCaiJingService iJinSeCaiJingService;

    @Test
    public void testJinSeCaiJing() throws Exception {
        JinSeCaiJing jinSeCaiJing = new JinSeCaiJing();
        jinSeCaiJing.setAuthor("justin");
        jinSeCaiJing.setContent("15465154554646");
        jinSeCaiJing.setDate("2018-09-06");
        jinSeCaiJing.setTags("php");
        jinSeCaiJing.setTitle("55665566");
        int i = iJinSeCaiJingService.add(jinSeCaiJing);
        System.out.println(i);
    }
}
